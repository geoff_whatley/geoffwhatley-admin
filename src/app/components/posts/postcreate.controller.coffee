angular.module "admin"
  .controller "PostCreateController", ($scope, $state, Post, Category, SessionService, AlertService) ->
    $scope.post = new Post()
    $scope.post.categories = []
    $scope.selectedCategories = []
    
    $scope.addPost = ->
      $scope.post.user_id = SessionService.userId()
      for category in $scope.selectedCategories
        $scope.post.categories.push category.id
      $scope.post.$save (response) ->
        AlertService.add('Post has been created.', 'success')
        $state.go 'viewPost', {id: response.data}