angular.module "admin"
  .controller "PostEditController", ($scope, $state, $stateParams, postPromise, AlertService, SessionService  ) ->
    $scope.post = postPromise
    $scope.selectedCategories = []

    $scope.deletePost = ->
      $scope.post.$delete ->
        AlertService.add('Post has been deleted permanently.', 'success')
        $state.go 'dashboard'

    $scope.savePost = ->
      $scope.post.user_id = SessionService.userId()
      $scope.post.categories = []
      for category in $scope.selectedCategories
        $scope.post.categories.push category.id
      $scope.post.$update (response) ->
        AlertService.add('Post has been saved.', 'success')
        $state.go 'viewPost', {id: response.data}