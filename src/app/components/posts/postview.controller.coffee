angular.module "admin"
  .controller "PostViewController", ($scope, $state, $stateParams, Post, Category, ModalService, AlertService) ->

    $scope.post = Post.get({id: $stateParams.id})
    $scope.categories = Category.query()

    $scope.deletePost = ->
      $scope.post.$delete ->
        AlertService.add('Post has been deleted permanently.', 'success')
        $state.go 'dashboard'

        