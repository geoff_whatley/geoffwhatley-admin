angular.module "admin"
  .controller "DashboardController", ($scope, $rootScope, Post, ModalService, AlertService) ->
    $rootScope.totalPosts
    $rootScope.postsPerPage
    $scope.pagination =
      current: 1

    $scope.posts = Post.query({page: 1})

    $scope.deletePost = (post, index) ->
      $scope.deletedTitle = post.title
      post.$delete ->
        AlertService.add('Post ' + $scope.deletedTitle + ' has been deleted permanently.', 'success')
        $scope.posts.splice(index, 1)
        $scope.posts = Post.query({page: $scope.pagination.current})

    $scope.pageChanged = (newPage) ->
      $scope.posts = Post.query({page: newPage})