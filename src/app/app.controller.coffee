angular.module "admin"
  .controller "AppController", ($rootScope, $scope, $state, AuthService) ->
    $scope.isAuthenticated = AuthService.isAuthenticated
    #$scope.isLoginPage = if $state.includes 'login' then true else false

    $rootScope.$on '$stateChangeSuccess', (event, toState, toParams, fromState, fromParams) ->
      $scope.isLoginPage = toState.name.indexOf('login') > -1
    
    #logic here to retrieve our app's settings and prettyify our app with branding etc
    $scope.appName = 'Evocate'
    $scope.appUrl = 'http://geoffwhatley.com'
    $scope.apiUrl = 'http://api.geoffwhatley.com'