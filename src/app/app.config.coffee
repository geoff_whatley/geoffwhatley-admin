angular.module "admin"
  .config ($stateProvider) ->
    $stateProvider
      .state 'login',
        url: "/login"
        templateUrl: "app/components/login/login.html"
      .state 'dashboard',
        url: "/dashboard"
        templateUrl: "app/components/dashboard/dashboard.html"
        controller: "DashboardController"
      .state 'newPost',
        url: "/posts/new"
        templateUrl: "app/components/posts/postcreate.html"
        controller: "PostCreateController"
      .state 'viewPost',
        url: "/posts/:id"
        templateUrl: "app/components/posts/postview.html"
        controller: "PostViewController"
      .state 'editPost',
        url: "/posts/:id/edit"
        resolve:
          postResource: 'Post',
          postPromise: (Post, $stateParams) ->
            Post.get({id: $stateParams.id}).$promise
        templateUrl: "app/components/posts/postedit.html"
        controller: "PostEditController"
      .state 'otherwise',
        url: "*path"
        templateUrl: "app/components/login/login.html"
        controller: "LoginController"