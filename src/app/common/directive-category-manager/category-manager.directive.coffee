angular.module "admin"
  .directive 'categoryManager', (Category) ->
    restrict: 'E'
    templateUrl: 'app/common/directive-category-manager/category-manager.html'
    link: (scope) ->

      scope.allCategories = Category.query()
      scope.filteredCategories = []

      for category in scope.post.categories
        scope.selectedCategories.push category
      
      scope.searchCategory = (term) ->
        scope.filteredCategories = []
        for category in scope.allCategories
          if category.name.toUpperCase().indexOf(term.toUpperCase()) >= 0
            scope.filteredCategories.push category
        scope.filteredCategories

      scope.selectCategory = (item) ->
        selectedCategory = {}

        if !!item.name
          selectedCategory = {'id': item.id, 'name': item.name}
          scope.selectedCategories.push selectedCategory
        else
          scope.category = new Category()
          scope.category.name = item.label
          scope.category.$save().then((response) ->
            selectedCategory = {'id': response.data.id, 'name': response.data.name}
            scope.selectedCategories.push selectedCategory
            scope.allCategories = Category.query()
          , (error) ->
            AlertService.add('Error creating new category', 'danger')
          )
        angular.element('#searchbox').text('')
        true

      scope.deselectCategory = (category) ->
        idx = scope.selectedCategories.indexOf(category)
        scope.selectedCategories.splice(idx, 1)