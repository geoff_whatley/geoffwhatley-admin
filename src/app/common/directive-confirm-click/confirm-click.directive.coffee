angular.module "admin"
  .directive "confirmClick", ($q, ModalService) ->
    link: (scope, element, attrs) ->
      ngClick = attrs.ngClick.replace('confirmClick()', 'true').replace('confirmClick(', 'confirmClick(true,')

      scope.confirmClick = (msg) ->
        if msg == true
          true
        msg = msg || attrs.confirmClick || 'Are you sure?'

        ModalService(msg).result.then ->
          scope.$eval(ngClick)

        false