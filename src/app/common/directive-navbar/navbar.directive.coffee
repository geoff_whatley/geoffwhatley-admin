angular.module "admin"
  .directive 'navbar', (AuthService) ->
    restrict: 'E'
    templateUrl: 'app/common/directive-navbar/navbar.html'
    link: (scope) ->
      scope.logout = ->
        AuthService.logout()