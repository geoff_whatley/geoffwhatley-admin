angular.module "admin"
  .directive 'loginDialog', ($state, AUTH_EVENTS, AuthService) ->
    restrict: 'E'
    templateUrl: 'app/common/directive-login-dialog/login-dialog.html'
    link: (scope, element, attrs) ->
      scope.modal = `'modal' in attrs`

      scope.login = (credentials) ->
        AuthService.login(credentials).then((user) ->
          #success
        ->
          #fail
        )

      showDialog = ->
        scope.visible = true

      hideDialog = ->
        scope.visible = false

      scope.visible = false

      if !!$state.includes('login')
        scope.visible = true

      scope.$on AUTH_EVENTS.notAuthenticated, showDialog
      scope.$on AUTH_EVENTS.sessionTimeout, showDialog
      scope.$on AUTH_EVENTS.loginSuccess, hideDialog

