angular.module "admin"
  .factory "AlertService", ($rootScope) ->
    add: (message, type) ->
      $rootScope.alert =
        message: message
        type: type
    clear: ->
      $rootScope.alert = ''