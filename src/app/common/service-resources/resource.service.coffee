angular.module "admin"
  .factory "ResourceService", () ->
    unwrapData: (data) ->
      wrappedData = angular.fromJson(data)
      unwrappedData = wrappedData.data
      unwrappedData