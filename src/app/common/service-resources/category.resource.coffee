angular.module "admin"
  .factory "Category", ($resource, $rootScope, ResourceService) ->
    ###
    Example Category Object {
    
    }
    ###
    $resource(
      'http://api.geoffwhatley.com/v1/categories/:id',
      {id: '@id'},
      'query':
        method: 'GET'
        transformResponse: (data) ->
          categories = ResourceService.unwrapData(data)
        isArray: true
      'delete':
        method: 'DELETE'
        url: 'http://api.geoffwhatley.com/v1/categories/:id'
      'get':
        'method': 'GET'
        transformResponse: (data) ->
          ResourceService.unwrapData(data)
      )