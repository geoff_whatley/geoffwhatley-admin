angular.module "admin"
  .factory "Post", ($resource, $rootScope, ResourceService) ->
    ###
    Example Post Object {
    id: "1"
    created_at: "2014-12-18 23:10:18"
    title: "Eum est."
    preview: "Cupiditate quasi et quia sunt doloremque."
    url: "eumest"
    created_at: "2014-12-18 23:10:18"
    updated_at: "2014-12-18 23:10:18"
    user_id: "1"
    }
    ###
    $resource(
      'http://api.geoffwhatley.com/v1/posts/:id',
      {id: '@id'},
      'query':
        method: 'GET'
        transformResponse: (data) ->
          posts = ResourceService.unwrapData(data)
          $rootScope.totalPosts = posts.pagination.total_records
          $rootScope.postsPerPage = posts.pagination.per_page
          posts.data
        isArray: true
      'delete':
        method: 'DELETE'
        #url: 'http://api.geoffwhatley.com/v1/posts/:id'
      'get':
        method: 'GET'
        transformResponse: (data) ->
          ResourceService.unwrapData(data)
      'update':
        method: 'PUT'
      )