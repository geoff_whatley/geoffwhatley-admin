angular.module "admin"
  .run ($rootScope, $injector, AuthService) ->
    $injector.get('$http').defaults.transformRequest = (data, headersGetter) ->
      if AuthService.isAuthenticated()
        headersGetter()['X-Auth-Token'] = AuthService.getToken()
      if data
        angular.toJson data