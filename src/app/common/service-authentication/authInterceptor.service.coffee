angular.module "admin"
  .factory 'AuthInterceptor', ($rootScope, $q, AUTH_EVENTS) ->
    responseError: (response) ->
      $rootScope.$broadcast(
        401: AUTH_EVENTS.notAuthenticated
        419: AUTH_EVENTS.sessionTimeout
        440: AUTH_EVENTS.sessionTimeout
        [response.status], response)
      $q.reject response