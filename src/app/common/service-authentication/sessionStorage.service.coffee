angular.module "admin"
  .factory "SessionStorageService", ->
    get: (key) ->
      sessionStorage.getItem(key)
    set: (key, val) ->
      sessionStorage.setItem(key, val)
    unset: (key) ->
      sessionStorage.removeItem(key)