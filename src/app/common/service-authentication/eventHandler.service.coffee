angular.module "admin"
  .run ($rootScope, $state, AUTH_EVENTS, AlertService, SessionService) ->
    $rootScope.$on(AUTH_EVENTS.loginSuccess, ->
      AlertService.clear()
      
      targetState = SessionService.getTargetState()

      if !!targetState.state
        $state.go targetState.state.name, targetState.params
      else
        $state.go 'dashboard'
    )

    $rootScope.$on(AUTH_EVENTS.loginFailed, ->
      AlertService.add('Login failed. Please check your credentials and try again.', 'warning')
    )

    $rootScope.$on(AUTH_EVENTS.logoutSuccess, ->
      SessionService.clearTargetState()
      $state.go('login')
      AlertService.add('Succesfully logged out', 'success')
    )

    $rootScope.$on(AUTH_EVENTS.logoutFailed, ->
      AlertService.add('Error logging out. Please try again.', 'danger')
    )

    $rootScope.$on(AUTH_EVENTS.sessionTimeout, ->
      SessionService.destroy()
      AlertService.add('Session has timed out. Please login to continue.', 'info')
    )

    $rootScope.$on(AUTH_EVENTS.notAuthenticated, ->
      SessionService.destroy()
      AlertService.add('Not authenticated. Please login to continue.', 'warning')
    )
