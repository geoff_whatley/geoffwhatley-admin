angular.module "admin"
  .run ($rootScope, $state, SessionService, AuthService, AUTH_EVENTS) ->
    $rootScope.$on('$stateChangeStart', (event, toState, toParams, fromState, fromParams) ->
      if toState.name isnt 'login' and toState.name isnt 'otherwise'
        SessionService.setTargetState toState, toParams

      if !AuthService.isAuthenticated()
        $rootScope.$broadcast AUTH_EVENTS.notAuthenticated
        if toState.name isnt 'login'
          event.preventDefault()
          if fromState.name is ''
            $state.go 'login'

      if AuthService.isAuthenticated()
        if toState.name is 'otherwise' or toState.name is 'login'
          event.preventDefault()
          $state.go 'dashboard'
    )