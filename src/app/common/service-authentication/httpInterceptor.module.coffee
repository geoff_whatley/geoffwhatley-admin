angular.module "admin"
  .config ($httpProvider) ->
    $httpProvider.interceptors.push(
      '$injector',
      ($injector) ->
        $injector.get('AuthInterceptor'))