angular.module "admin"
  .factory "SessionService", (SessionStorageService) ->
    create: (userId, token) ->
      SessionStorageService.set('authenticated', true)
      SessionStorageService.set('userId', userId)
      SessionStorageService.set('token', token)

    destroy: ->
      SessionStorageService.unset('authenticated')
      SessionStorageService.unset('userId')
      SessionStorageService.unset('token')

    authenticated: ->
      SessionStorageService.get('authenticated')

    token: ->
      SessionStorageService.get('token')

    userId: ->
      SessionStorageService.get('userId')

    setTargetState: (state, params) ->
      SessionStorageService.set('state', JSON.stringify(state))
      SessionStorageService.set('params', JSON.stringify(params))

    getTargetState: ->
      targetState = {}
      targetState['state'] = angular.fromJson SessionStorageService.get 'state'
      targetState['params'] = angular.fromJson SessionStorageService.get 'params'
      targetState

    clearTargetState: ->
      SessionStorageService.unset('state')
      SessionStorageService.unset('params')