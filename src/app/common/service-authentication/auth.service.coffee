angular.module "admin"
  .factory "AuthService", ($http, $rootScope, SessionService, AUTH_EVENTS) ->

    login: (credentials) ->
      $http(
        method: 'POST'
        url: 'http://api.geoffwhatley.com/auth/login'
        params: credentials)
      .then((response) ->
        SessionService.create(response.data.user.id, response.data.token)
        $rootScope.$broadcast AUTH_EVENTS.loginSuccess
        response.user
      ,
      (response) ->
        $rootScope.$broadcast AUTH_EVENTS.loginFailed
      )

    isAuthenticated: ->
      !!SessionService.authenticated()

    getToken: ->
      SessionService.token()

    logout: ->
      $http(
        'method': 'DELETE'
        'url': 'http://api.geoffwhatley.com/auth/logout')
      .then((response) ->
        SessionService.destroy()
        $rootScope.$broadcast AUTH_EVENTS.logoutSuccess
      ,
      (response) ->
        $rootScope.$broadcast AUTH_EVENTS.logoutFailure
      )