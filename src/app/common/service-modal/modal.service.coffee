angular.module "admin"
  .service "ModalService", ($modal) ->
    (message, title, okButton, cancelButton) ->
      okButton = if okButton == false then false else (okButton || 'Confirm')
      cancelButton = if cancelButton == false then false else (cancelButton || 'Cancel')

      ModalInstanceController = ($scope, $modalInstance, settings) ->
        angular.extend($scope, settings)

        $scope.ok = ->
          $modalInstance.close true

        $scope.cancel = ->
          $modalInstance.dismiss 'cancel'

      modalInstance = $modal.open(
        template: '<div class="dialog-modal"> \
                    <div class="modal-header" ng-show="modalTitle"> \
                      <h3 class="modal-title">{{modalTitle}}</h3> \
                    </div> \
                    <div class="modal-body">{{modalBody}}</div> \
                    <div class="modal-footer"> \
                      <button class="btn btn-primary" ng-click="ok()" ng-show="okButton">{{okButton}}</button> \
                      <button class="btn btn-warning" ng-click="cancel()" ng-show="cancelButton">{{cancelButton}}</button> \
                    </div> \
                  </div>',
        controller: ModalInstanceController
        resolve:
          settings: ->
            modalTitle: title
            modalBody: message
            okButton: okButton
            cancelButton: cancelButton
      )

      modalInstance